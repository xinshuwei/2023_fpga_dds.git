module da_wave_send(
	input	clk_i,	
	input	rst_n_i, 
	input   [31:0] freq_set, //freaq set 
	input   [1:0] wave_type,
	input   [3:0] wave_gain,
	input	[9:0]	sin_data_i, //sin ROM read 
	input   [9:0]   tri_data_i, //tri ROM read
	output	[9:0]	rd_addr_o, //read rom addr
	//DA
	output	da_clk_o, //DA clk
	output	reg [9:0]	da_data_o //output da data
	);
parameter FREQ_ADJ = 10'd0; //range 0~1023  FREQ_ADJ large ,out freq low
reg [9:0] freq_cnt; //freq cnt
reg [31:0] phase_acc; //phase cnt
wire [9:0] square_wave;

//427
assign square_wave=phase_acc[31]?10'd43:10'h00; 
//data send posedge  data sample negedge
assign da_clk_o = ~clk_i;
//rom addr
assign rd_addr_o = phase_acc[31:22];
//freq calc
always @(posedge clk_i or negedge rst_n_i) begin
	if(rst_n_i == 1'b0)
		freq_cnt <= 10'b0;
	else if(freq_cnt == FREQ_ADJ)
		freq_cnt <= 10'd0;
	else
		freq_cnt <= freq_cnt + 10'd1;
end

//phase addr
always @(posedge clk_i or negedge rst_n_i) begin
	if(rst_n_i == 1'b0)
		phase_acc <= 31'b0;
	else begin
		if(freq_cnt == FREQ_ADJ) begin
			phase_acc <= phase_acc + freq_set;
		end
	end
end


always @(* ) begin
		case (wave_type)
		2'd0: begin    da_data_o =sin_data_i*wave_gain ;end
		2'd1: begin    da_data_o =square_wave*wave_gain ;end
		2'd2: begin    da_data_o =tri_data_i*wave_gain ;end
		default:  begin da_data_o =1'b0;end
		endcase
end
endmodule
