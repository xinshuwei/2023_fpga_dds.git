module spi_slave(
clk, 
rst_n,
SCK, 
MOSI, 
MISO, 
SSEL, 
freq_set,
wave_type,
wave_gain
);
input clk;
input rst_n;
input SCK, SSEL, MOSI;

output MISO;
output reg [31:0]freq_set;
output reg [1:0]wave_type;
output reg [3:0]wave_gain;


//----------------------------------------------------------------------------------------
// 3bit get sck edges
reg [2:0] SCKr;  always @(posedge clk) SCKr <= {SCKr[1:0], SCK};
wire SCK_risingedge = (SCKr[2:1]==2'b01);  // now we can detect SCK rising edges
wire SCK_fallingedge = (SCKr[2:1]==2'b10);  // and falling edges

// get  cs
reg [2:0] SSELr;  always @(posedge clk) SSELr <= {SSELr[1:0], SSEL};
wire SSEL_active = ~SSELr[1];  // SSEL is active low
wire SSEL_startmessage = (SSELr[2:1]==2'b10);  // message starts at falling edge
wire SSEL_endmessage = (SSELr[2:1]==2'b01);  // message stops at rising edge

// get mosi data in
reg [1:0] MOSIr;  always @(posedge clk) MOSIr <= {MOSIr[0], MOSI};
wire MOSI_data = MOSIr[1];
//----------------------------------------------------------------------------------------

//recv
//----------------------------------------------------------------------------------------
reg [2:0] bitcnt;
reg [31:0] byte_data_received; //recv reg��32bit

always @(posedge clk or negedge rst_n)
begin
  if(~rst_n)
	byte_data_received <= 32'd0;
  else
  if(~SSEL_active)
    bitcnt <= 3'b000;
  else
  if(SCK_risingedge)
  begin
    bitcnt <= bitcnt + 3'b001;
    // implement a shift-left register (since we receive the data MSB first)
    byte_data_received <= {byte_data_received[31:0], MOSI_data};
  end
end



//reg byte_received;  // high when a byte has been received
//always @(posedge clk) byte_received <= SSEL_active && SCK_risingedge && (bitcnt==3'b111);

// data recive
//assign freq_set = byte_data_received;
/*****
* DataRecv[9:0]    freq control   0---999  10
* DataRecv[11:10]  freq uint  00 1Hz  01 KHz 10 MHz  11 not use 2 
* DataRecv[15:12]  wavegain   1 --0.1V  2 0.2V 3--0.3V  ..... 10 1V   4
* DataRecv[17:16]  waveType   00 sin  01 tri  10  squre  11 not use 2
* DataRecv[31:18]
*****/

always @(posedge clk) 
begin
	if(SSEL_endmessage==1)//message recv finish
		begin
		//uint sel
		case  (byte_data_received[11:10])
			//1Hz
			2'b00:begin freq_set=72*byte_data_received[9:0]; wave_gain =byte_data_received[15:12];wave_type= byte_data_received[17:16];end
			//1KHz
			2'b01:begin freq_set=72000*byte_data_received[9:0];wave_gain =byte_data_received[15:12];wave_type= byte_data_received[17:16];end
			//1MHz
			2'b10:begin freq_set=72000000*byte_data_received[9:0];wave_gain =byte_data_received[15:12];wave_type= byte_data_received[17:16];end
			default:begin freq_set=72*byte_data_received[9:0]; wave_gain =byte_data_received[15:12];wave_type= byte_data_received[17:16];end
		endcase
		end
end





//send data
//----------------------------------------------------------------------------------------
reg [7:0] byte_data_sent;

reg [7:0] cnt;
always @(posedge clk) if(SSEL_startmessage) cnt<=cnt+8'h1;  // count the messages

always @(posedge clk)
if(SSEL_active)
begin
  if(SSEL_startmessage)
    byte_data_sent <= cnt;  // first byte sent in a message is the message count
  else
  if(SCK_fallingedge)
  begin
    if(bitcnt==3'b000)
      byte_data_sent <= 8'h00;  // after that, we send 0s
    else
      byte_data_sent <= {byte_data_sent[6:0], 1'b0};
  end
end

assign MISO = byte_data_sent[7];  // send MSB first
//----------------------------------------------------------------------------------------

endmodule