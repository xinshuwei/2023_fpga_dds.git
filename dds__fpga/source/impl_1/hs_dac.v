module hs_dac(
	input	clk_12M, //system clk 12m
	input   key_rst_n, //reset in
	
	//spi_slave
	input   SCK, //SCK data
	input   SSEL, //cs data
	input   MOSI, //MOSI data
	output  MISO, //MISO data
	//DAC port
	output  da_clk, //DAC clk
	output  [9:0]  da_data, //DAC data
	//led output
	output  reg  led1
	);
	
	
wire clk_60M; //60MHzʱ��
wire rst_i; //btn rst in (high enabel)
wire [9:0] rd_addr_i; //ROM addr
wire [9:0] sin_data_o; //sin ROM data
wire [9:0] tri_data_o; //tri ROM data
wire [31:0]freq_set; //freq step 
wire [3:0] wav_gain;
wire [1:0] wav_type;







reg [24:0] led_cnt; //LEDcnt


//pll
pll_60M u_pll_60M(
	.ref_clk_i(clk_12M),
    .rst_n_i(key_rst_n),
    .outcore_o(clk_60M),
    .outglobal_o( )
	);

//led
always @(posedge clk_60M) begin
	led_cnt <= led_cnt + 1'b1;
	if(led_cnt==25'd30000000) led1 = ~led1;
end

//reset
assign rst_i = ~key_rst_n;

//tri rom

tri_rom u_tri_rom(
	.rd_clk_i(clk_60M),
    .rst_i(rst_i),
    .rd_en_i( ),
    .rd_clk_en_i( ),
    .rd_addr_i(rd_addr_i),
    .rd_data_o(tri_data_o)
	);
	
//sin rom
rom_1024x10b u_rom_1024x10b(
	.rd_clk_i(clk_60M),
    .rst_i(rst_i),
    .rd_en_i( ),
    .rd_clk_en_i( ),
    .rd_addr_i(rd_addr_i),
    .rd_data_o(sin_data_o)
	);

//dac out control
da_wave_send u_da_wave_send(
	.clk_i(clk_60M),
	.rst_n_i(key_rst_n),
	.freq_set(freq_set),
	.wave_type(wav_type),
	.wave_gain(wav_gain),
	.sin_data_i(sin_data_o),
	.tri_data_i(tri_data_o),
	.rd_addr_o(rd_addr_i),
	.da_clk_o(da_clk),
	.da_data_o(da_data)
	);

//spi_slave
spi_slave u_spi_slave(
	.clk(clk_60M),
	.rst_n(key_rst_n),
	.SCK(SCK),
	.SSEL(SSEL),
	.MOSI(MOSI),
	.MISO(MISO),
	.freq_set(freq_set),
	.wave_type(wav_type),
	.wave_gain(wav_gain)
	);

endmodule