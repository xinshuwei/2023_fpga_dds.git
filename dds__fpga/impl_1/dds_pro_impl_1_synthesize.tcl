if {[catch {

# define run engine funtion
source [file join {G:/ProgramData/lscc/radiantide} scripts tcl flow run_engine.tcl]
# define global variables
global para
set para(gui_mode) 1
set para(prj_dir) "G:/win10/Desktop/Embedded/FPGA/lattice/mycode/dds_pro_fpga"
# synthesize IPs
# synthesize VMs
# propgate constraints
file delete -force -- dds_pro_impl_1_cpe.ldc
run_engine_newmsg cpe -f "dds_pro_impl_1.cprj" "pll_60M.cprj" "rom_1024x10b.cprj" "tri_rom.cprj" -a "iCE40UP"  -o dds_pro_impl_1_cpe.ldc
# synthesize top design
file delete -force -- dds_pro_impl_1.vm dds_pro_impl_1.ldc
run_engine_newmsg synthesis -f "dds_pro_impl_1_lattice.synproj"
run_postsyn [list -a iCE40UP -p iCE40UP5K -t SG48 -sp High-Performance_1.2V -oc Industrial -top -w -o dds_pro_impl_1_syn.udb dds_pro_impl_1.vm] "G:/win10/Desktop/Embedded/FPGA/lattice/mycode/dds_pro_fpga/impl_1/dds_pro_impl_1.ldc"

} out]} {
   runtime_log $out
   exit 1
}
