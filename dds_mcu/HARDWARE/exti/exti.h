#ifndef __exti_H
#define __exti_H

#include "stm32g031xx.h"

void EXTI_Init(void);


enum
{
	WAV_FREQ_HZ=0,
	WAV_FREQ_10HZ,
	WAV_FREQ_100HZ,
	WAV_FREQ_KHZ,
	WAV_FREQ_10KHZ,
	WAV_FREQ_100KHZ,
	WAV_FREQ_MHZ,
	WAV_FREQ_NUM
};
enum Freq_Uint
{
	FREQ_HZ=0,
	FREQ_KHZ,
	FREQ_MHZ
};


enum WAV_TYPE
{
	SIN_WAVE=0,
	TRI_WAVE,
	SQUARE_WAVE,
	WAVE_TYPE_NUM
};

enum ROW_SEL
{
	NO_SEL_ROW=0,
	TYPE_ROW ,
	FREQ_ROW,
	STEP_ROW,
	GAIN_ROW,
	ROW_NUM
};

#endif
