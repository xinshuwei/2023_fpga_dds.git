#include "exti.h"
#include "key.h"
#include "stm32g0xx_hal.h"
#include "led.h"
#include "encoder.h"
#include "oled.h"
#include "stdio.h"
#include "math.h"

 //read key1  
#define  KEY1_READ HAL_GPIO_ReadPin(key_GPIO_Port, key1_Pin) 
//read key2
#define  KEY2_READ HAL_GPIO_ReadPin(key_GPIO_Port, key2_Pin)  
//read B
#define  EC11_B_READ  HAL_GPIO_ReadPin(encoder_GPIO_Port,encoderB_Pin)


const char wav_uint_name[WAV_FREQ_NUM][30]=
{
	"1Hz   \0",
	"10Hz  \0",
	"100Hz \0",
	"1KHz  \0",
	"10KHz \0",
	"100KHz\0",
	"1MHz  \0"
};

typedef struct 
{
	u32 freq_code:10;
	u32 freq_uint:2;
	u32 wave_gain:4;
	u32 wave_type:2;
	u32 unused:14;
}DDSData;

const char wav_gain_name[10][30]=
{
	"0.1V\0",
	"0.2V\0",
	"0.3V\0",
	"0.4V\0",
	"0.5V\0",
	"0.6V\0",
	"0.7V\0",
	"0.8V\0",
	"0.9V\0",
	"1.0V\0"
};

static u8 cur_wav_gain = 9;//cur wave gain
static u8 cur_wav_type = SIN_WAVE;//cur wav type
static u8 cur_row_mode = NO_SEL_ROW;//change mode
static u8 step_flag =WAV_FREQ_KHZ;// cur wav uint step flag;
extern u32 freq; //cur  wav freq


extern SPI_HandleTypeDef hspi1;//hw spi

static uint32_t wav_freq_uint=1000;
static uint16_t freq_code;//send to fpga freq code params
static uint32_t freq_uint;//send to fpga freq uint params
static DDSData dds_data;//send to fpga data params
static u8 EC11_B_State; //ec11 b read sate


//send to fpga params
static void fpga_spi_send(void)
{
			switch(wav_freq_uint)
			{
				case 1:
				case 10:
				case 100:  freq_uint =0;freq_code = freq; break;
					
				case 1000:
				case 10000:
				case 100000:freq_uint=1;freq_code= freq/1000;  break;
					
				case 1000000:freq_uint=2;	freq_code=freq/1000000; break;
				
				default: break;
			}
			dds_data.freq_code = freq_code;
			dds_data.freq_uint = freq_uint;
			dds_data.wave_gain = cur_wav_gain+1;//map  1~~~10 
			dds_data.wave_type = cur_wav_type;
			dds_data.unused = 0;
			uint8_t* p_data = (uint8_t*)&dds_data;
			for(uint8_t i=4;i>0;i--)
			{
				HAL_SPI_Transmit(&hspi1,&p_data[i-1],1,1000);
			}
}


static void clear_row(void)
{
	
	for(u8 i=0;i<4;i++)
	{
		 OLED_ShowChar(102,i*16,' ',16);
	   OLED_ShowChar(110,i*16,' ',16);
	   OLED_ShowChar(118,i*16,' ',16);
		
	}
}
void draw_sel_mode(u8 idx)
{
	
	clear_row();
	switch(idx)
	{
		case TYPE_ROW:
		case FREQ_ROW:
		case STEP_ROW:			
		case GAIN_ROW:
		OLED_ShowChar(102,(idx-1)*16,'<',16);
	  OLED_ShowChar(110,(idx-1)*16,'=',16);
	  OLED_ShowChar(118,(idx-1)*16,'=',16);
		break;
		default:break;
	}
	OLED_Refresh();
}


//oled update freq
static void oled_update_freq(void)
{
			OLED_Clear1();
			draw_sel_mode(cur_row_mode);
			SetCursor(40,16);
			if(freq<1000)  printf("%3dHz",freq);
			else if((freq>1000)&&(freq<1000000))
			{
				printf("%3dkHz",freq/1000);
			}
			else
			{
				printf("%dMHz",freq/1000000);
			}
			OLED_Refresh();
}



static void EC11_update_gain(u8 flag)
{
	if(flag ==1)//A negedge
		{
			if(EC11_B_State)//A下降沿，B高电平，顺时针
			{
					cur_wav_gain ++;
			}
			else //A下降沿，B低电平，逆时针
			{
				  cur_wav_gain --;
			}
		}
		else //A posedge
		{
			if(!EC11_B_State) //A上升沿，B低电平，下降沿
			{
					cur_wav_gain ++;
			}
			else //A上升沿，B高电平，逆时针
			{
				cur_wav_gain --;
			}
		}
		
		cur_wav_gain = cur_wav_gain%10;
		SetCursor(40,48);
		printf("%s",wav_gain_name[cur_wav_gain]);
	  OLED_Refresh();
}

static void EC11_update_mode(u8 flag)
{
		if(flag ==1)//A negedge
		{
			if(EC11_B_State)//A下降沿，B高电平，顺时针
			{
					cur_wav_type ++;
			}
			else //A下降沿，B低电平，逆时针
			{
				  cur_wav_type --;
			}
		}
		else //A posedge
		{
			if(!EC11_B_State) //A上升沿，B低电平，下降沿
			{
					cur_wav_type ++;
			}
			else //A上升沿，B高电平，逆时针
			{
				cur_wav_type --;
			}
		}
		cur_wav_type=cur_wav_type %WAVE_TYPE_NUM;
		
		switch(cur_wav_type)
		{
			case SIN_WAVE: 	  OLED_ShowChinese(40,0,0,16);OLED_ShowChinese(56,0,1,16);break;
	    case TRI_WAVE:    OLED_ShowChinese(40,0,13,16);OLED_ShowChinese(56,0,14,16);break;
	    case SQUARE_WAVE: OLED_ShowChinese(40,0,11,16);OLED_ShowChinese(56,0,12,16);break;
			default:break;
		}
		OLED_Refresh();	
}




static void EC11_update_uint(u8 flag)
{
		if(flag ==1)//A negedge
		{
			if(EC11_B_State)//A下降沿，B高电平，顺时针
			{
					step_flag ++;
			}
			else //A下降沿，B低电平，逆时针
			{
				  step_flag --;
			}
		}
		else //A posedge
		{
			if(!EC11_B_State) //A上升沿，B低电平，下降沿
			{
					step_flag ++;
			}
			else //A上升沿，B高电平，逆时针
			{
				step_flag --;
			}
		}
		step_flag=step_flag %WAV_FREQ_NUM;
		wav_freq_uint = pow(10,step_flag);
		freq = wav_freq_uint;
		SetCursor(40,32);
		printf("%s",wav_uint_name[step_flag]);
		OLED_Refresh();	
}


static void EC11_update_freq(u8 flag)
{
	  if(flag ==1)//A negedge
		{
			if(EC11_B_State)//A下降沿，B高电平，顺时针
			{
				freq+=wav_freq_uint;
				if(freq>10000000) freq=10000000;
			}
			else //A下降沿，B低电平，逆时针
			{
				freq -=wav_freq_uint;
				if(freq>10000000) freq=0;
			}
		}
		else //A posedge
		{
			if(!EC11_B_State) //A上升沿，B低电平，下降沿
			{
				 freq +=wav_freq_uint;
				if(freq>10000000) freq=10000000;
			}
			else //A上升沿，B高电平，逆时针
			{
				freq -=wav_freq_uint;
				if(freq>10000000) freq=0;
			}
		}
		oled_update_freq();
}

//中断服务程序中需要做的事情
//在HAL库中所有的外部中断服务函数都会调用此函数
//GPIO_Pin:中断引脚号
void HAL_GPIO_EXTI_Falling_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
		case key1_Pin: //复位按键
		{
			if(KEY1_READ==0)
			{
				HAL_Delay(10);
				if(KEY1_READ==0)
				{
					
					OLED_Clear1();
					draw_sel_mode(cur_row_mode);
					
					SetCursor(40,16);
					step_flag = WAV_FREQ_KHZ;
					wav_freq_uint = pow(10,step_flag);
					freq = wav_freq_uint; 
					printf("%dkHz",freq/wav_freq_uint);
					SetCursor(40,32);
					printf("%s",wav_uint_name[step_flag]);
					
					OLED_Refresh();
				}
				
			}
			break;
		}
		case key2_Pin: //切换频率步进按键
		{
			if(KEY2_READ==0)
			{
				HAL_Delay(10);
				if(KEY2_READ==0)
				{

					
					cur_row_mode++;
					cur_row_mode = cur_row_mode%ROW_NUM;
					draw_sel_mode(cur_row_mode);
					
				}
			}
			break;
		}
		case encoderA_Pin: //A negedge
		{
			EC11_B_State = EC11_B_READ;
			switch (cur_row_mode)
			{
				case TYPE_ROW:EC11_update_mode(1);break;
				case FREQ_ROW:EC11_update_freq(1);break;
				case STEP_ROW:EC11_update_uint(1);break;			
				case GAIN_ROW:EC11_update_gain(1);break;
				default:break;
			}
			
			break;
		}
		
		case encoder_key_Pin: //编码器按键，改变频率
		{
			fpga_spi_send();
			break;
		}
		default:	break;
	}
}




void HAL_GPIO_EXTI_Rising_Callback(uint16_t GPIO_Pin)
{
	switch(GPIO_Pin)
	{
		case encoderA_Pin:
		{
			EC11_B_State = EC11_B_READ;
			switch (cur_row_mode)
			{
				case TYPE_ROW:EC11_update_mode(0);break;
				case FREQ_ROW:EC11_update_freq(0);;break;
				case STEP_ROW:EC11_update_uint(0);break;			
				case GAIN_ROW:EC11_update_gain(0);break;
				default:break;
			}
			break;
		}
		default:	break;
	}
}


//外部中断初始化
void EXTI_Init(void)
{
    GPIO_InitTypeDef GPIO_Initure = {0};
    
    __HAL_RCC_GPIOA_CLK_ENABLE();               //开启GPIOA时钟
		__HAL_RCC_GPIOB_CLK_ENABLE();               //开启GPIOB时钟
		__HAL_RCC_GPIOC_CLK_ENABLE();               //开启GPIOC时钟
    
    GPIO_Initure.Pin=key1_Pin|key2_Pin; 	//PA11,PA12
    GPIO_Initure.Mode=GPIO_MODE_IT_FALLING;     //下降沿触发
    GPIO_Initure.Pull=GPIO_PULLUP;
    HAL_GPIO_Init(key_GPIO_Port,&GPIO_Initure);
		
		GPIO_Initure.Pin=encoderA_Pin; 	//PB0
		GPIO_Initure.Mode=GPIO_MODE_IT_RISING_FALLING;     //上升下降沿触发
		HAL_GPIO_Init(encoder_GPIO_Port,&GPIO_Initure);
		
		GPIO_Initure.Pin=encoder_key_Pin; 	//PC6
    GPIO_Initure.Mode=GPIO_MODE_IT_FALLING;     //下降沿触发
    HAL_GPIO_Init(encoder_key_Port,&GPIO_Initure);
		
		//中断线0-PB0
    HAL_NVIC_SetPriority(EXTI0_1_IRQn,1,0);       //抢占优先级为2，子优先级为0
    HAL_NVIC_EnableIRQ(EXTI0_1_IRQn);             //使能中断线2
    
    //中断线11-PA11
    HAL_NVIC_SetPriority(EXTI4_15_IRQn,2,0);       //抢占优先级为2，子优先级为0
    HAL_NVIC_EnableIRQ(EXTI4_15_IRQn);             //使能中断线2
}

//中断服务函数
void EXTI0_1_IRQHandler(void)
{
	HAL_GPIO_EXTI_IRQHandler(encoderA_Pin);		//调用中断处理公用函数
}

//中断服务函数
void EXTI4_15_IRQHandler(void)
{
	HAL_Delay(10);
	if(HAL_GPIO_ReadPin(key_GPIO_Port,key1_Pin) == GPIO_PIN_RESET) //key1下降沿
	{
    HAL_GPIO_EXTI_IRQHandler(key1_Pin);		//调用中断处理公用函数
	}
	else if(HAL_GPIO_ReadPin(key_GPIO_Port,key2_Pin) == GPIO_PIN_RESET) //key2下降沿
	{
		HAL_GPIO_EXTI_IRQHandler(key2_Pin);		//调用中断处理公用函数
	}
	else if(HAL_GPIO_ReadPin(encoder_key_Port,encoder_key_Pin) == GPIO_PIN_RESET) //编码器按键下降沿
	{
		HAL_GPIO_EXTI_IRQHandler(encoder_key_Pin);		//调用中断处理公用函数
	}
}
