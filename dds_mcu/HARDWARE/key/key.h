#ifndef __key_H
#define __key_H

#include "stm32g031xx.h"
 
#define key_GPIO_Port GPIOA //定义key端口
#define key1_Pin	GPIO_PIN_11 //定义key1管脚
#define key2_Pin	GPIO_PIN_12 //定义key2管脚

void KEY_Init(void);

#endif
