#include "key.h"
#include "stm32g0xx_hal.h"

/*******************************************************************************
* 函 数 名         : KEY_Init
* 函数功能		   : 按键初始化
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/
void KEY_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure = {0} ; //定义结构体变量	
	__HAL_RCC_GPIOA_CLK_ENABLE();        //开启GPIOA时钟
	
	GPIO_InitStructure.Pin=key1_Pin|key2_Pin; 
  GPIO_InitStructure.Mode=GPIO_MODE_INPUT;  //输入模式
  GPIO_InitStructure.Pull=GPIO_PULLUP;      //上拉
  HAL_GPIO_Init(key_GPIO_Port,&GPIO_InitStructure);
}
