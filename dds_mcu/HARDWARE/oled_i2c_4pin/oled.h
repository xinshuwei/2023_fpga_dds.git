#ifndef __OLED_H
#define __OLED_H

#include "stm32g031xx.h"
#include "stm32g0xx_hal.h"

typedef uint8_t u8;
typedef uint32_t u32;

//-------------------OLED端口定义-------------------------
//PA0
#define SCL_GPIO_Port GPIOA
#define SCL_Pin GPIO_PIN_0
#define OLED_SCL_Clr() HAL_GPIO_WritePin(SCL_GPIO_Port,SCL_Pin,GPIO_PIN_RESET)
#define OLED_SCL_Set() HAL_GPIO_WritePin(SCL_GPIO_Port,SCL_Pin,GPIO_PIN_SET)

//PA4
#define SDA_GPIO_Port GPIOA
#define SDA_Pin GPIO_PIN_4
#define OLED_SDA_Clr() HAL_GPIO_WritePin(SDA_GPIO_Port,SDA_Pin,GPIO_PIN_RESET)
#define OLED_SDA_Set() HAL_GPIO_WritePin(SDA_GPIO_Port,SDA_Pin,GPIO_PIN_SET)

//PA5
#define RES_GPIO_Port GPIOA
#define RES_Pin GPIO_PIN_5
#define OLED_RES_Clr() HAL_GPIO_WritePin(RES_GPIO_Port,RES_Pin,GPIO_PIN_RESET)
#define OLED_RES_Set() HAL_GPIO_WritePin(RES_GPIO_Port,RES_Pin,GPIO_PIN_SET)

//PA8
#define DC_GPIO_Port GPIOA
#define DC_Pin GPIO_PIN_8

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

//函数定义
//初始化相关
void I2C_Start(void);
void I2C_Stop(void);
void I2C_WaitAck(void);
void Send_Byte(u8 dat);
void OLED_WR_Byte(u8 dat, u8 mode);
void OLED_Refresh(void);
void OLED_Clear(void);
void OLED_Init(void);

//显示相关
void OLED_ColorTurn(u8 i);
void OLED_DisplayTurn(u8 i);
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_WR_BP(u8 x, u8 y);

//画图相关
void OLED_DrawPoint(u8 x,u8 y);
void OLED_ClearPoint(u8 x,u8 y);
void OLED_DrawLine(u8 x1,u8 y1,u8 x2,u8 y2);
void OLED_DrawCircle(u8 x,u8 y,u8 r);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size1);
void OLED_ShowString(u8 x,u8 y,u8 *chr,u8 size1);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size1);
void OLED_ShowChinese(u8 x,u8 y,u8 num,u8 size1);
void OLED_ScrollDisplay(u8 num,u8 space);
void OLED_ShowPicture(u8 x0,u8 y0,u8 x1,u8 y1,u8 BMP[]);

#endif
