#ifndef __OLED_H
#define __OLED_H

#include "stm32g031xx.h"
#include "stm32g0xx_hal.h"

typedef uint8_t u8;
typedef uint32_t u32;

//-------------------OLED端口定义-------------------------
//PA0
#define SCL_GPIO_Port GPIOA
#define SCL_Pin GPIO_PIN_0
#define OLED_SCL_Clr() HAL_GPIO_WritePin(SCL_GPIO_Port,SCL_Pin,GPIO_PIN_RESET)
#define OLED_SCL_Set() HAL_GPIO_WritePin(SCL_GPIO_Port,SCL_Pin,GPIO_PIN_SET)

//PA4
#define SDA_GPIO_Port GPIOA
#define SDA_Pin GPIO_PIN_4
#define OLED_SDA_Clr() HAL_GPIO_WritePin(SDA_GPIO_Port,SDA_Pin,GPIO_PIN_RESET)
#define OLED_SDA_Set() HAL_GPIO_WritePin(SDA_GPIO_Port,SDA_Pin,GPIO_PIN_SET)

//PA5
#define RES_GPIO_Port GPIOA
#define RES_Pin GPIO_PIN_5
#define OLED_RES_Clr() HAL_GPIO_WritePin(RES_GPIO_Port,RES_Pin,GPIO_PIN_RESET)
#define OLED_RES_Set() HAL_GPIO_WritePin(RES_GPIO_Port,RES_Pin,GPIO_PIN_SET)

//PA8
#define DC_GPIO_Port GPIOA
#define DC_Pin GPIO_PIN_8
#define OLED_DC_Clr() HAL_GPIO_WritePin(DC_GPIO_Port,DC_Pin,GPIO_PIN_RESET)
#define OLED_DC_Set() HAL_GPIO_WritePin(DC_GPIO_Port,DC_Pin,GPIO_PIN_SET)

#define OLED_CMD  0	//写命令
#define OLED_DATA 1	//写数据

#define SCREEN_COLUMN 128

//函数定义
//初始化相关
void OLED_WR_Byte(u8 dat, u8 mode);
void OLED_Refresh(void);
void OLED_Clear(void);
void OLED_Clear1(void);
void OLED_Init(void);

//显示相关
void OLED_ColorTurn(u8 i);
void OLED_DisplayTurn(u8 i);
void OLED_Display_On(void);
void OLED_Display_Off(void);
void OLED_WR_BP(u8 x, u8 y);

//画图相关
void OLED_DrawPoint(u8 x,u8 y);
void OLED_ClearPoint(u8 x,u8 y);
void OLED_ShowChar(u8 x,u8 y,u8 chr,u8 size1);
void OLED_ShowString(u8 x,u8 y,u8 *chr,u8 size1);
void OLED_ShowNum(u8 x,u8 y,u32 num,u8 len,u8 size1);
void OLED_ShowChinese(u8 x,u8 y,u8 num,u8 size1);

//打印信息相关
void SetCursor(int x, int y);

#endif
