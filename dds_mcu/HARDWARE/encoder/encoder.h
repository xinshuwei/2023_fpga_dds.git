#ifndef __encoder_H
#define __encoder_H

#include "stm32g031xx.h"
 
#define encoder_GPIO_Port GPIOB //定义编码器端口
#define encoderA_Pin GPIO_PIN_0 //定义A口管脚
#define encoderB_Pin GPIO_PIN_1 //定义B口管脚

#define encoder_key_Port GPIOC //定义按键端口
#define encoder_key_Pin GPIO_PIN_6 //定义按键引脚

void ENCODER_Init(void);

#endif
