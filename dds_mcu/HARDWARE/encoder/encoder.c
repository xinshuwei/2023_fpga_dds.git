#include "encoder.h"
#include "stm32g0xx_hal.h"

/*******************************************************************************
* 函 数 名         : ENCODER_Init
* 函数功能		   : 编码器初始化
* 输    入         : 无
* 输    出         : 无
*******************************************************************************/
void ENCODER_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure = {0} ; //定义结构体变量
	
	__HAL_RCC_GPIOB_CLK_ENABLE();        //开启GPIOB时钟
	__HAL_RCC_GPIOC_CLK_ENABLE();        //开启GPIOC时钟
	
	GPIO_InitStructure.Pin=encoderA_Pin|encoderB_Pin; 
  GPIO_InitStructure.Mode=GPIO_MODE_INPUT;  //输入模式
  GPIO_InitStructure.Pull=GPIO_PULLUP;      //上拉
  HAL_GPIO_Init(encoder_GPIO_Port,&GPIO_InitStructure);
}
