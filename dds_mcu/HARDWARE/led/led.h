#ifndef __led_H
#define __led_H

#include "stm32g031xx.h"

#define led_GPIO_Port GPIOB
#define led_Pin GPIO_PIN_8

void LED_Init(void);

#endif
